/**
  * Created by mark on 13/03/2017.
  */
import Questions._
import org.scalatest.FunSuite
/** 15%
  * 請寫出一個次方函數,pow(x,y)
  * ex:
  * pow(2,3)=2*2*2
  * pow(3,2)=3*3
  **/
class PowSpec extends FunSuite{
  test("Pow(2,3)=8") {
    assert(pow(2,3)==8)

  }

  test("Pow(3,2)=9") {
    assert(pow(3,2)==9)
  }
  test("Pow(11,-2)=0.008264462809917356") {
    assert(pow(11,-2)==0.008264462809917356)

  }

  test("Pow(9,-4)=1.5241579027587256E-4") {
    assert(pow(9,-4)==1.5241579027587256E-4)
  }
  test("Pow(1,0)=1") {
    assert(pow(1,0)==1)
  }


}
