/**
  * Created by mark on 13/03/2017.
  */
import Questions._
import org.scalatest.FunSuite
/** 15%
  * 請用Tail Recursive寫出階乘函數fac(n)=n*(n-1)*...*2*1
  * ex:
  * fib(5)=120
  **/


class TailFacSpec extends FunSuite{
  test("tailFac(5,1)=120") {
    assert(tailFac(5,1)==120)

  }
  test("tailFac(7,1)=5040") {
    assert(tailFac(7,1)==5040)

  }
  test("tailFac(10,1)=3628800") {
    assert(tailFac(10,1)==3628800)

  }
  test("tailFac(10000000,1)") {
    assert(tailFac(10000000,1)==0)

  }


}

